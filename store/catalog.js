const types = {
  SET_CATALOG: 'SET_CATALOG',
  SET_CATALOG_NEW_ITEM: 'SET_CATALOG_NEW_ITEM'
}

export const state = () => ({
  catalog: []
})

export const getters = {
  getCatalog: state => state.catalog.map((item) => {
    const dt = new Date(+item.date)
    const date = `${dt.getDate()}/${dt.getMonth() + 1}/${dt.getFullYear()}`

    return { ...item, date }
  })
}

export const mutations = {
  [types.SET_CATALOG] (state, { data }) {
    state.catalog = data
  },
  [types.SET_CATALOG_NEW_ITEM] (state, newItem) {
    state.catalog.unshift(newItem)
  }
}

export const actions = {
  async loadCatalog ({ commit }) {
    const data = await this.$axios.$get(process.env.baseURL + '/data.json')

    if (data) {
      commit(types.SET_CATALOG, { data })
    }
  },
  addNewItem ({ commit }, newItem) {
    commit(types.SET_CATALOG_NEW_ITEM, newItem)
  },
  removeItem ({ commit, state }, id) {
    commit(types.SET_CATALOG, { data: state.catalog.filter(item => item.id !== id) })
  },
  changeItem ({ commit, state }, newItem) {
    const oldItemIndex = state.catalog.findIndex(item => item.id === newItem.id)
    const catalog = [].concat(state.catalog)

    catalog.splice(oldItemIndex, 1, newItem)
    commit(types.SET_CATALOG, { data: catalog })
  }
}
